﻿using System;

namespace week_3_ex16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*******************************");
            Console.WriteLine("****  Wellcome to my app   ****");
            Console.WriteLine("*******************************");
            Console.WriteLine("");
            Console.WriteLine("*******************************");
            Console.WriteLine("What is you name?");
            var name= Console.ReadLine();
            Console.WriteLine($"Your name is {name}");
        }
    }
}
